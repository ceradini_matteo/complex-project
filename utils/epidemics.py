import networkx as nx
import csv
import numpy as np
import math
import random
import copy

class EpidemicsSimulator:
    def __init__(self, graph, num_nodes):
        self.G = graph
        self.num_nodes = num_nodes
        
    def percolation(self, beta, trials=10):
        avg_edges = 0
        avg_connected_components = 0
        avg_largest_cc = 0
        
        for t in range(0, trials):
            G_1 = copy.deepcopy(self.G)

            for n in range(0, self.num_nodes):
                for edge in self.G.edges(str(n)):
                    random_decimal = random.randint(0, 99)/100
                    
                    if random_decimal > beta and G_1.has_edge(str(n),str(edge[1])):
                        G_1.remove_edge(str(n),str(edge[1]))
                        
            avg_edges += G_1.size()
            avg_connected_components += nx.number_connected_components(G_1)
            avg_largest_cc += len(max(nx.connected_components(G_1), key=len))
            
        avg_edges /= trials
        avg_connected_components /= trials
        avg_largest_cc /= trials
        
        return avg_edges, avg_connected_components, avg_largest_cc

    def __get_susceptibles_nodes(self):
        num_s = 0

        for n in range(0, self.num_nodes):
            if self.G.nodes[str(n)]['status'] == 'S':
                num_s += 1

        return num_s

    def __get_infected_nodes(self, update_steps=False, model='SIS', u2_steps=0):
        num_i = 0

        for n in range(0, self.num_nodes):
            if self.G.nodes[str(n)]['status'] == 'I':
                num_i += 1

                if update_steps:
                    self.G.nodes[str(n)]['infection_duration'] -= 1

                    if self.G.nodes[str(n)]['infection_duration'] == 0:
                        if model == 'SIS':
                            self.G.nodes[str(n)]['status'] = 'S'
                        else:
                            self.G.nodes[str(n)]['status'] = 'R'
                            self.G.nodes[str(n)]['immunization_duration'] = u2_steps

        return num_i

    def __get_removed_nodes(self, update_steps=False):
        num_r = 0

        for n in range(0, self.num_nodes):
            if self.G.nodes[str(n)]['status'] == 'R':
                num_r += 1

                if update_steps:
                    self.G.nodes[str(n)]['immunization_duration'] -= 1

                    if self.G.nodes[str(n)]['immunization_duration'] == 0:
                        self.G.nodes[str(n)]['status'] = 'S'

        return num_r

    def __infect_others(self, source_node, beta, u_steps):
        neighbors = self.G.neighbors(str(source_node))

        for u in neighbors:
            if self.G.nodes[str(u)]['status'] == 'S':
                random_decimal = random.randint(0, 99)/100

                if random_decimal < beta:
                    self.G.nodes[str(u)]['status'] = 'I'
                    self.G.nodes[str(u)]['infection_duration'] = u_steps

    def sis_model(self, beta, u_steps, time_steps):
        s = np.zeros([time_steps])
        i = np.zeros([time_steps])

        # initialization of of nodes status
        for n in range(0, self.num_nodes):
            self.G.nodes[str(n)]['status'] = 'S'
            self.G.nodes[str(n)]['infection_duration'] = 0
            self.G.nodes[str(n)]['immunization_duration'] = 0

        # at first time step there are only susceptibles nodes
        s[0] = self.num_nodes
        i[0] = 0

        # determine at second time how many nodes (random choosen) will be initially infected (using prob beta)
        for n in range(0, self.num_nodes):
            random_decimal = random.randint(0, 99)/100

            if random_decimal <= beta:
                self.G.nodes[str(n)]['status'] = 'I'
                self.G.nodes[str(n)]['infection_duration'] = u_steps

        s[1] = self.__get_susceptibles_nodes()
        i[1] = self.__get_infected_nodes()

        # propagate the infection for all the time_steps
        for time_step in range(2, time_steps):
            for n in range(0, self.num_nodes):
                if self.G.nodes[str(n)]['status'] == 'I':
                    self.__infect_others(n, beta, u_steps)

            s[time_step] = self.__get_susceptibles_nodes()
            i[time_step] = self.__get_infected_nodes(update_steps=True, model='SIS')


        return s,i

    def sirs_model(self, beta, u1_steps, u2_steps, time_steps):
        s = np.zeros([time_steps])
        i = np.zeros([time_steps])
        r = np.zeros([time_steps])

        # initialization of of nodes status
        for n in range(0, self.num_nodes):
            self.G.nodes[str(n)]['status'] = 'S'
            self.G.nodes[str(n)]['infection_duration'] = 0

        # at first time step there are only susceptibles nodes
        s[0] = self.num_nodes
        i[0] = 0
        r[0] = 0

        # determine at second time how many nodes (random choosen) will be initially infected (using prob beta)
        for n in range(0, self.num_nodes):
            random_decimal = random.randint(0, 99)/100

            if random_decimal <= beta:
                self.G.nodes[str(n)]['status'] = 'I'
                self.G.nodes[str(n)]['infection_duration'] = u1_steps

        s[1] = self.__get_susceptibles_nodes()
        i[1] = self.__get_infected_nodes()
        r[1] = self.__get_removed_nodes()

        # propagate the infection for all the time_steps
        for time_step in range(2, time_steps):
            for n in range(0, self.num_nodes):
                if self.G.nodes[str(n)]['status'] == 'I':
                    self.__infect_others(n, beta, u1_steps)

            s[time_step] = self.__get_susceptibles_nodes()
            i[time_step] = self.__get_infected_nodes(update_steps=True, model='SIRS', u2_steps=u2_steps)
            r[time_step] = self.__get_removed_nodes(update_steps=True)

        return s,i,r