import networkx as nx
import matplotlib.pyplot as plt
import csv
import numpy as np
import math
import collections
import random

def generate_random_color():
    return "#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])

class GraphManager:
    def __init__(self, graph, num_nodes):
        self.G = graph
        self.num_nodes = num_nodes
        self.primary_color = '#663399'
        self.secondary_color = '#4daf7c'
        self.tertiary_color = '#f7ca18'

    def assortativity(self, k_nn_k):
        x = np.fromiter(k_nn_k.keys(), dtype=float)
        y = np.fromiter(k_nn_k.values(), dtype=float)
        fig, ax = plt.subplots()
        ax.loglog(x, y, color=self.primary_color, marker='o', markersize=5, linestyle="dotted")
        ax.set(xlabel='k', ylabel='knn(k)', title='Assortativity graph')
        ax.grid()
        plt.show()

    def degree_distribution(self, nodes_degree_distribution):
        items = sorted(nodes_degree_distribution.items())
        x = sorted(nodes_degree_distribution.keys())
        y = np.zeros([len(x)])

        for index, val in enumerate(items):
            y[index] = len(val[1])

        plt.subplot(211)
        plt.plot(x, y, color=self.primary_color, marker='.')
        plt.xlabel('degree') 
        plt.ylabel('count') 
        plt.title("Degree distribution")
        plt.subplot(212)
        plt.loglog(x, y, color=self.primary_color, marker='.')
        plt.xlabel('degree') 
        plt.ylabel('count') 
        plt.title("Degree distribution (log-log plot)")
        plt.show()

    def degree_centrality(self, degree_centrality):
        degree_centrality_distribution = {}

        for dc in degree_centrality:
            if dc in degree_centrality_distribution:
                degree_centrality_distribution[dc] += 1
            else:
                degree_centrality_distribution[dc] = 1

        items = sorted(degree_centrality_distribution.items())
        x_distribution = sorted(degree_centrality_distribution.keys())
        y_distribution = np.zeros([len(x_distribution)])

        for index, val in enumerate(items):
            y_distribution[index] = val[1]

        x = np.arange(self.num_nodes)
        plt.subplot(211)
        plt.plot(x, degree_centrality, color=self.primary_color)
        plt.xlabel('nodes') 
        plt.ylabel('C_d(u)') 
        plt.title("Degree centrality") 
        plt.subplot(212)
        plt.plot(x_distribution, y_distribution, color=self.primary_color, marker='.', markersize=5)
        plt.xlabel('C_d(u)') 
        plt.ylabel('count') 
        plt.title("Degree centrality distribution") 
        plt.show()

    def betweenness_centrality(self, betweenness_centrality):
        x = np.arange(self.num_nodes)
        y = []

        for item in betweenness_centrality.values():
            y.append(item)

        plt.plot(x, y, color=self.primary_color)
        plt.xlabel('nodes') 
        plt.ylabel('C_b(u)') 
        plt.title("Betweenness centrality")
        plt.show()

    def correlation_bc_degree(self, max_bc_degrees):
        x = np.zeros([len(max_bc_degrees)])
        y = np.zeros([len(max_bc_degrees)])

        for index, n in enumerate(max_bc_degrees):
            x[index] = index + 1
            y[index] = self.G.degree[str(n)]

        plt.bar(x, y, color=self.primary_color)
        plt.xlabel('highest bc centrality nodes') 
        plt.ylabel('degree') 
        plt.title("Correlation betweenness centrality and degree")
        plt.xticks(np.arange(1, len(max_bc_degrees) + 1))
        plt.show()

    def closeness_centrality(self, closeness_centrality):
        x = np.arange(self.num_nodes)
        y = []

        for item in closeness_centrality.values():
            y.append(item)

        closeness_centrality_distribution = {}

        for cc in closeness_centrality:
            if closeness_centrality[cc] in closeness_centrality_distribution:
                closeness_centrality_distribution[closeness_centrality[cc]] += 1
            else:
                closeness_centrality_distribution[closeness_centrality[cc]] = 1

        items = sorted(closeness_centrality_distribution.items())
        x_distribution = sorted(closeness_centrality_distribution.keys())
        y_distribution = np.zeros([len(x_distribution)])

        for index, val in enumerate(items):
            y_distribution[index] = val[1]

        plt.subplot(211)
        plt.bar(x, y, color=self.primary_color)
        plt.xlabel('nodes') 
        plt.ylabel('C_c(u)') 
        plt.title("Closeness centrality") 
        plt.subplot(212)
        plt.plot(x_distribution, y_distribution, color=self.primary_color)
        plt.xlabel('C_d(u)') 
        plt.ylabel('count') 
        plt.title("Closeness centrality distribution") 
        plt.show()
        
    def clustering_coefficient(self, clustering_coefficient):
        x = np.arange(self.num_nodes)
        items = {}
        
        for c1 in clustering_coefficient:
            if c1 in items:
                items[c1] += 1
            else:
                items[c1] = 1
        
        x_distribution = sorted(items)
        y_distribution = []
        
        for c1 in x_distribution:
            y_distribution.append(items[c1])
        
        plt.subplot(211)
        plt.plot(x, clustering_coefficient, color=self.primary_color)
        plt.xlabel('nodes') 
        plt.ylabel('C1') 
        plt.title("Clustering coefficient") 
        plt.subplot(212)
        plt.plot(x_distribution, y_distribution, color=self.primary_color)
        plt.xlabel('C_d(u)') 
        plt.ylabel('count') 
        plt.title("Clustering coefficient distribution") 
        plt.show()
        
        
    def communities(self, partition):
        communities = {}
        
        for node, c in partition.items():
            if c in communities:
                communities[c].append(node)
            else:
                communities[c] = []
                communities[c].append(node)
                
        communities_edges = {}
        
        for c in range(0, len(communities)):
            communities_edges[c] = np.zeros([len(communities)])
         
        for c1 in range(0, len(communities)):   
            for c2 in range(0, len(communities)):
                if c1 != c2:
                    for node_c1 in communities[c1]:
                        for node_c2 in communities[c2]:
                            if self.G.has_edge(str(node_c1), str(node_c2)):
                                communities_edges[c1][c2] += 0.5
                                communities_edges[c2][c1] += 0.5
                    
        communities_graph = nx.Graph()
        
        for c1 in range(0, len(communities)):
            for c2,value in enumerate(communities_edges[c1]):
                if value > 0:
                    communities_graph.add_edge(c1, c2, weight=value)

        
        pos = nx.shell_layout(communities_graph)
        plt.figure() 
        
        color_map = []
        nodes_size = []
        
        for c in range(0, len(communities)):
            color_map.append(generate_random_color())
            nodes_size.append(sum(communities_edges[c]))
            
        max_node_size = np.amax(nodes_size)
        
        # normalization
        for c in range(0, len(communities)):
            nodes_size[c] = 1900 * nodes_size[c] / max_node_size
        
        nx.draw(
            communities_graph,
            pos,
            node_color=color_map,
            node_size=nodes_size,
            edge_color='black',
            width=1,
            linewidths=1,
            labels={node:node for node in communities_graph.nodes()}
        )
            
        formatted_edge_labels = {(elem[0],elem[1]):round(elem[2]['weight']) for elem in communities_graph.edges().data()}
        
        nx.draw_networkx_edge_labels(
            communities_graph,
            pos,
            edge_labels=formatted_edge_labels
        )
        plt.show()
        
    def homophily_analysis(self, class_prob, class_edge_hom):
        # convert in probabilities
        class_prob *= 100
        class_edge_hom *= 100
        
        x = np.arange(len(class_prob))
        
        plt.bar(x, class_prob, color=self.primary_color, width = 0.30)
        plt.bar(x + 0.30, class_edge_hom, color=self.secondary_color, width = 0.30)
        plt.xlabel('Class') 
        plt.ylabel('%') 
        plt.title("Same class homophily analysis") 
        plt.xticks(np.arange(0, len(class_prob)))
        plt.legend(['nodes probability', 'same class homophily'])
        plt.show()
        
    # EPIDEMICS PLOTS

    def percolation_results(self, beta_range, avg_edges, avg_connected_components, avg_largest_cc):
        plt.subplot(311)
        plt.plot(beta_range, avg_edges, color=self.primary_color)
        plt.ylabel('Avg. edges') 
        plt.title("Percolation results") 
        plt.subplot(312) 
        plt.plot(beta_range, avg_connected_components, color=self.primary_color)
        plt.ylabel('Avg. connected components') 
        plt.subplot(313) 
        plt.plot(beta_range, avg_largest_cc, color=self.secondary_color)
        plt.xlabel('Beta') 
        plt.ylabel('Avg. largest cc size')         
        plt.show()

    def sis_model(self, time_steps, beta, results_s, results_i):
        x = np.arange(time_steps)

        plt.subplot(311)
        
        plt.plot(x, results_s[0], color=self.primary_color, label='S')
        plt.plot(x, results_i[0], color=self.secondary_color, label='I')
        plt.legend(); 
        plt.ylabel('Nodes') 
        plt.title("SIS MODEL WITH beta = {}".format(beta)) 
        plt.subplot(312) 
        plt.plot(x, results_s[1], color=self.primary_color, label='S')
        plt.plot(x, results_i[1], color=self.secondary_color, label='I')
        plt.legend(); 
        plt.ylabel('Nodes') 
        plt.subplot(313) 
        plt.plot(x, results_s[2], color=self.primary_color, label='S')
        plt.plot(x, results_i[2], color=self.secondary_color, label='I')
        plt.legend(); 
        plt.xlabel('Steps') 
        plt.ylabel('Nodes')         
        plt.show()

    def sir_model(self, time_steps, beta, results_s, results_i, results_r):
        x = np.arange(time_steps)

        plt.subplot(311)
        
        plt.plot(x, results_s[0], color=self.primary_color, label='S')
        plt.plot(x, results_i[0], color=self.secondary_color, label='I')
        plt.plot(x, results_r[0], color=self.tertiary_color, label='R')
        plt.legend(); 
        plt.ylabel('Nodes') 
        plt.title("SIR MODEL WITH beta = {}".format(beta)) 
        plt.subplot(312) 
        plt.plot(x, results_s[1], color=self.primary_color, label='S')
        plt.plot(x, results_i[1], color=self.secondary_color, label='I')
        plt.plot(x, results_r[1], color=self.tertiary_color, label='R')
        plt.legend(); 
        plt.ylabel('Nodes') 
        plt.subplot(313) 
        plt.plot(x, results_s[2], color=self.primary_color, label='S')
        plt.plot(x, results_i[2], color=self.secondary_color, label='I')
        plt.plot(x, results_r[2], color=self.tertiary_color, label='R')
        plt.legend(); 
        plt.xlabel('Steps') 
        plt.ylabel('Nodes')         
        plt.show()