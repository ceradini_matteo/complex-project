import networkx as nx
import csv
import numpy as np
import math
import community as community_louvain
import json
import heapq
from operator import itemgetter

''' BIG DATASET FIRST ANALYSIS
Num. nodes:  7624
Num. edges:  27806
Density:  0.0009568849118596328
Avg. degree:  7.294333683105981
Avg. path length:  5.232237268915701
Avg. path length compare to the log(N): 5.232237268915701 vs 8.939056445334039
Diameter:  15
'''

class CalculusManager:
    def __init__(self, graph, num_nodes):
        self.G = graph
        self.num_nodes = num_nodes
        
    def heterogeneity_parameter(self, degree_avg):
        k = 0

        for n in range(0, self.num_nodes):
            k += math.pow(self.G.degree[str(n)], 2)
        
        k /= self.num_nodes

        return k / (math.pow(degree_avg, 2))
    
    def retrieve_variances(self, degree_avg):
        squared_diff = 0

        for i in range(0, self.num_nodes):
            squared_diff += (math.pow(self.G.degree[str(i)], 2) - degree_avg)   
            
        variance = squared_diff / self.num_nodes
        standard_deviation = math.sqrt(variance)
        
        return variance, standard_deviation        

    def shortest_paths_length(self):
        path_length_matrix = np.zeros([self.num_nodes, self.num_nodes]) # in this matrix each cell (i,j) represents the shortest path distance between the nodes i,j

        for i in range(0, self.num_nodes):
            for j in range(0, self.num_nodes):
                if i != j and path_length_matrix[i,j] == 0:
                    sp = nx.algorithms.shortest_path(self.G, source=str(i), target=str(j))
                    path_length_matrix[i, j] = len(sp) - 1
                    path_length_matrix[j, i] = len(sp) - 1    

        sp_sum = np.sum(path_length_matrix) / 2 # divide by 2 because we sum 2 times each distance

        avg_path_length = 2 * sp_sum / (self.num_nodes * (self.num_nodes - 1))

        diameter = np.amax(path_length_matrix)        

        print("\nAvg. path length: ", avg_path_length)
        print("Avg. path length compare to the log(N): {} vs {}".format(avg_path_length, math.log(self.num_nodes)))
        print("Diameter: ", round(diameter))

        return avg_path_length, path_length_matrix, diameter

    def graph_assortativity(self):
        nodes_degree = {}
        nodes_degree_singular = {}

        for index in range(0, len(self.G)):
            nodes = self.G.degree[str(index)]
            nodes_degree_singular[index] = nodes

            if nodes in nodes_degree:
                nodes_degree[nodes].append(index)
            else:
                nodes_degree[nodes] = [index]

        k_nn_ki = {}

        for index in range(0, len(self.G)):
            k_i = nodes_degree_singular[index]
            sum = 0

            for n in self.G.neighbors(str(index)):
                sum += nodes_degree_singular[int(n)]

            k_nn_ki[index] = (sum / k_i)

        max_k = max(nodes_degree)
        k_nn_k = {}

        for k in sorted(nodes_degree.keys()):
            sum = 0

            for index in nodes_degree[k]:
                sum += k_nn_ki[index]

            k_nn_k[k] = sum / len(nodes_degree[k])

        return k_nn_k, nodes_degree

    def degree_centrality(self):
        degree_centrality_abs = np.zeros([self.num_nodes])
        degree_centrality = np.zeros([self.num_nodes])

        for u in range(0, self.num_nodes):
            degree_centrality_abs[u] = self.G.degree[str(u)]

        max_node_degree = np.amax(degree_centrality_abs)

        for u in range(0, self.num_nodes):
            degree_centrality[u] = degree_centrality_abs[u] / max_node_degree

        return degree_centrality, degree_centrality_abs, max_node_degree

    def betweenness_centrality(self, load_from_file=True):
        bc_file_name = './data/betweenness_centrality.txt'

        if load_from_file:
            with open(bc_file_name, "r") as file:
                bc = json.loads(file.read())
        else:
            bc = nx.betweenness_centrality(self.G)
            
            with open(bc_file_name, "w") as f:
                f.write(json.dumps(bc))

        return bc

    def max_bc_degrees(self, betweenness_centrality, n=15):
        max_bc = dict(heapq.nlargest(n, betweenness_centrality.items(), key=itemgetter(1)))
        
        return max_bc

    def closeness_centrality(self, load_from_file=True):
        cc_file_name = './data/closeness_centrality.txt'
        
        if load_from_file:
            with open(cc_file_name, "r") as file:
                cc = json.loads(file.read())
        else:
            cc = nx.closeness_centrality(self.G)
            
            with open(cc_file_name, "w") as f:
                f.write(json.dumps(cc))

        return cc

    def edge_beetweenness(self, load_from_file=True):
        eb_file_name = './data/edge_beetweenness.txt'    
        
        if load_from_file:
            with open(eb_file_name, "r") as file:
                eb = json.loads(file.read())
        else:
            eb = nx.edge_betweenness_centrality(self.G)
            
            with open(eb_file_name, "w") as f:
                f.write(json.dumps(eb))

        return eb
    
    def clustering_coefficient_c1(self):
        c1_items = np.zeros([self.num_nodes])

        for v in range(0, self.num_nodes):
            k_v = self.G.degree[str(v)]
            
            if k_v > 0:
                neighbors = [n for n in self.G.neighbors(str(v))]
                N_v = 0
                
                for node1 in neighbors:
                    for node2 in neighbors:
                        if self.G.has_edge(node1,node2):
                            N_v += 1
                            
                N_v /= 2 # because we sum 2 times each edge
                
                if N_v > 0:
                    c1 = (N_v / (0.5*k_v*(k_v-1)))
                    c1_items[v] = c1
                
        avg_c1 = np.sum(c1_items) / self.num_nodes
            
        return avg_c1, c1_items
    
    def clustering_coefficient_c2(self):
        return nx.transitivity(self.G)
    
    def compute_eigens(self):
        ''' an alternative way:
        L = nx.linalg.laplacianmatrix.laplacian_matrix(self.G)
        e = np.linalg.eigvals(L.A)
        '''  
    
        e = nx.linalg.spectrum.laplacian_spectrum(self.G)
        eigen_values = []
        
        for e_value in sorted(e, reverse=True):
            eigen_values.append(abs(round(e_value, 6)))
        
        return eigen_values
    
    def connected_components(self, eigen_values):
        connected_components = 0

        for e in eigen_values:
            if e == 0:
                connected_components += 1

        print("\nNumber of connected components: ", connected_components)
        
        return connected_components
    
    def communities_detection(self):
        partition = community_louvain.best_partition(self.G)       
        
        return partition

    def compute_homophily(self, target_dataset, num_classes=18):
        class_prob = np.zeros([num_classes])

        with open(target_dataset) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            next(csv_reader)
            index = 0

            for row in csv_reader:
                self.G.nodes[str(index)]['target'] = row[1]
                class_prob[int(row[1])] += 1
                index += 1
        
        class_prob /= self.num_nodes
        
        class_edge_hom = np.zeros([num_classes])

        for n in range(0, self.num_nodes):
            current_target = self.G.nodes[str(n)]['target'] 
            neighbors = self.G.neighbors(str(n))

            for u in neighbors:
                if self.G.nodes[str(u)]['target'] == current_target:
                    class_edge_hom[int(current_target)] += 1
                    
        class_edge_hom /= 2*self.G.size() # also / 2 because the edges are counted twice
        
        return class_prob, class_edge_hom

