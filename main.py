import networkx as nx
import matplotlib.pyplot as plt
import csv
import numpy as np
import math

from utils.calculus import CalculusManager
from utils.graph_plot import GraphManager
from utils.epidemics import EpidemicsSimulator

G = nx.Graph()

# STEP 0 : Create the Networkx graph representation with nodes and edges

dataset = 'dataset/lasftm_asia/edges.csv'

with open(dataset) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    for row in csv_reader:
        G.add_edge(row[0], row[1])

# STEP 1 : Calculate all the basic measures for the graph
num_nodes = len(G)
num_edges = G.size()

calc_manager = CalculusManager(G, num_nodes)
graph_manager = GraphManager(G, num_nodes)

max_num_edges = num_nodes * (num_nodes - 1) / 2
density = num_edges / max_num_edges
degree_avg = density * (num_nodes - 1)
heterogeneity_parameter = calc_manager.heterogeneity_parameter(degree_avg)
degree_variance, degree_standard_deviation = calc_manager.retrieve_variances(degree_avg)

print("Num. nodes: ", num_nodes)
print("Num. edges: ", num_edges)
print("\nDensity: ", density)
print("Degree avg.: ", degree_avg)
print("Heterogeneity parameter (k): ", heterogeneity_parameter)
print("Degree variance: ", degree_variance)
print("Degree standard deviation: ", degree_standard_deviation)

# retrieve shortest paths length
#avg_path_length, path_length_matrix, diameter = calc_manager.shortest_paths_length()

# calculate graph assortativity
k_nn_k, nodes_degree_distribution = calc_manager.graph_assortativity()
    
# centralities & betweenness

# centrality 1 => degree centrality
degree_centrality, nodes_degree, max_node_degree = calc_manager.degree_centrality()

print("Max node degree:", max_node_degree)

# centrality 2 => betweenness centrality
betweenness_centrality = calc_manager.betweenness_centrality(load_from_file=True)
max_bc_degrees = calc_manager.max_bc_degrees(betweenness_centrality)

# centrality 3 => closeness centrality
closeness_centrality = calc_manager.closeness_centrality(load_from_file=True)
print(closeness_centrality)

edge_beetweenness = calc_manager.edge_beetweenness(load_from_file=False)

# clustering coefficient 1
clustering_c1_avg, clustering_c1 = calc_manager.clustering_coefficient_c1()

# clustering coefficient 2
clustering_c2_avg = calc_manager.clustering_coefficient_c2()

print("\nClustering coefficient c1 avg.:", clustering_c1_avg)
print("Clustering coefficient c2 avg.:", clustering_c2_avg)

# computing eigen-values and determining the number of connected_components
eigen_values = calc_manager.compute_eigens()
connected_components = calc_manager.connected_components(eigen_values)

# communities detection
partition = calc_manager.communities_detection()

# homophily analysis
target_dataset = 'dataset/lasftm_asia/nodes_target.csv'

class_prob, class_edge_hom = calc_manager.compute_homophily(target_dataset)

# STEP 2 : plot all the measures useful for a better understanding of the network
graph_manager.degree_distribution(nodes_degree_distribution)

# assortativy graph
graph_manager.assortativity(k_nn_k)

# degree centrality graph
graph_manager.degree_centrality(degree_centrality)

# betweenness centrality graph
graph_manager.betweenness_centrality(betweenness_centrality)

# correlation betweenness centrality and nodes degree graph
graph_manager.correlation_bc_degree(max_bc_degrees)

# closeness centrality graph
graph_manager.closeness_centrality(closeness_centrality)

# clustering coefficient_1 distribution graph
graph_manager.clustering_coefficient(clustering_c1)

# plotting communities
graph_manager.communities(partition)

# innerclass homophily
graph_manager.homophily_analysis(class_prob, class_edge_hom)

# STEP 3 : epidemics analysis
print("------------------------------------------\nEPIDEMICS ANALYSIS:\n\n")

epidemics_simulator = EpidemicsSimulator(G, num_nodes)


print("SIR MODEL ANALYSIS WITH PERCOLATION\n")
result_avg_edges = []
result_avg_connected_components = []
result_avg_largest_cc = []
beta_range = np.arange(0.00, 1.01, 0.025)

for beta in beta_range:
    beta = round(beta, 3)

    print("beta = {}".format(beta))
    avg_edges, avg_connected_components, avg_largest_cc = epidemics_simulator.percolation(beta, trials=10)

    result_avg_edges.append(avg_edges)
    result_avg_connected_components.append(avg_connected_components)
    result_avg_largest_cc.append(avg_largest_cc)

graph_manager.percolation_results(beta_range, result_avg_edges, result_avg_connected_components, result_avg_largest_cc)

print("SIS MODEL ANALYSIS")
beta_range = [0.01, 0.10, 0.25, 0.40]
u_range = [5, 10, 15]
time_steps = 100

for beta in beta_range:
    print("\n-----------------\nbeta: {}\n".format(beta))
    
    results_s = []    
    results_i = []    

    for u in u_range:
        print("u: {}".format(u))

        s,i = epidemics_simulator.sis_model(beta, u, time_steps)
        
        results_s.append(s)
        results_i.append(i)
    
    graph_manager.sis_model(time_steps, beta, results_s, results_i)

print("SIR MODEL ANALYSIS")
beta_range = [0.01, 0.10, 0.25, 0.40]
u1 = 5
u2_range = [5, 10, 15]
time_steps = 100

for beta in beta_range:
    print("\n-----------------\nbeta: {}\n".format(beta))
    
    results_s = []    
    results_i = []    
    results_r = []    

    for u2 in u2_range:
        print("u2: {}".format(u2))

        s,i,r = epidemics_simulator.sirs_model(beta, u1, u2, time_steps)
        
        results_s.append(s)
        results_i.append(i)
        results_r.append(r)
    
    graph_manager.sir_model(time_steps, beta, results_s, results_i, results_r)